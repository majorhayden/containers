#!/bin/bash
set -euo pipefail

# Make a note of the package versions.
buildah --version

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

# Newer versions of podman/buildah try to set overlayfs mount options when
# using the vfs driver, and this causes errors.
sed -i '/^mountopt =.*/d' /etc/containers/storage.conf

# Build the container.
buildah bud --squash -f builds/${IMAGE_NAME} -t ${IMAGE_NAME} .

# If a tag was provided, use it for the image. If we're on the master branch,
# use 'latest'.
if [ ! -z "${CI_COMMIT_TAG:-}" ]; then
    IMAGE_TAG=${CI_COMMIT_TAG}
elif [ "$CI_COMMIT_REF_NAME" = 'master' ]; then
    IMAGE_TAG="latest"
fi

# For containers that should not be tagged, just update the latest.
if [[ "${IMAGE_TAG:-}" != "" ]] && [[ "${LATEST_ONLY:-}" = 'true' ]]; then
    IMAGE_TAG="latest"
fi

# If we are not on a tag or on the master branch, stop here. There is no need
# to push.
if [ -z "${IMAGE_TAG:-}" ]; then
    exit 0
fi

if [ -n "${CI_REGISTRY:-}" ]; then
    # Prepare to push to GitLab's container registry.
    export REGISTRY_AUTH_FILE=${HOME}/auth.json
    echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
    GITLAB_TAG=${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${IMAGE_TAG}
    buildah tag ${IMAGE_NAME} ${GITLAB_TAG}

    # Push to registry.gitlab.com.
    for i in {1..5}; do
        echo "Attempt #${i} to push to registry.gitlab.com..."
        if buildah push ${GITLAB_TAG}; then
            PUSH_GITLAB_OK=1
            break
        fi
    done
fi
